﻿uDAC-CS43198 USB Audio DA converter project
---

Lukas Otava 2018

![Mostly assembled PCBs of the uDAC-CS43198 USB audio converter](/doc/DSC_0529.JPG)

## Features
- UAC1 asynchronous transport based on STM32F048 ARM Cortex-M0 microcontroller
- 24 bit PCM audio at 44.1/48/88.2/96 kHz
- USB A connector
- Jack 3.5 line-out connector
- High performance audio converter CS43198 (or CS43131)
- Audio frequency clock source (crystal oscillator)
- 6 low noise LDOs
- 5 reconstruction filter switchable
- USB audio feature for volume control
- ST micro USB DFU firmware update

## HW design
- Current revision is A
- Two prototypes sucessfully tested (personal everyday use)
- Schematic hw_design\uDAC.pdf
- PCB of the prototype produced at OSHPark.org (https://oshpark.com/shared_projects/dFZLSq4q)
- Bill of material (hw_design\production_files\ibom.html)

## Firmware
- Current version 0.5 (beta)
- At time of writing - closed source
- Firmware in firmware_image folder
- Connecting USB to host holding MODE button will select DFU mode
- flashing
	- DfuSeDemo utility
	- dfu-util-static -d 0483:df11  -a 0 -i 0 -c 1 -s 0x08000000:leave -D uDAC_0_5.bin

## USB VID PID
- It is planned to use Openmoko USB Product IDs

## Tools used
- Kicad 5.1.10 EDA
- SW4STM32 IDE
- STM32CubeMX project configuration tool
- DfuSeDemo for firmware flashing

## Licence
- This hardware design is licenced under CERN Open Hardware Licence Version 2 - Weakly Reciprocal (see LICENSE.txt)
